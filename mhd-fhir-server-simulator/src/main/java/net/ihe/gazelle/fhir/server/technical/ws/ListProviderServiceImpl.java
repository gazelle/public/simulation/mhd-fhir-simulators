package net.ihe.gazelle.fhir.server.technical.ws;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.api.server.RequestDetails;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.gclient.IQuery;
import ca.uhn.fhir.rest.gclient.StringClientParam;
import jakarta.enterprise.context.ApplicationScoped;
import org.eclipse.microprofile.config.ConfigProvider;
import org.hl7.fhir.instance.model.api.IBaseBundle;
import org.hl7.fhir.r4.model.ListResource;

import java.util.List;
import java.util.Map;


@ApplicationScoped
public class ListProviderServiceImpl implements ListProviderService {


    private final IGenericClient client;

    public ListProviderServiceImpl() {
        FhirContext ctx = FhirContext.forR4(); // or the appropriate version
        String serverBase = ConfigProvider.getConfig().getValue("nist.fhir.toolkit.endpoint", String.class);
        client = ctx.newRestfulGenericClient(serverBase);
    }



    @Override
    public List<ListResource> searchListResource(RequestDetails request) {

        Map<String, String[]> parameterMap = request.getParameters();
        IQuery<IBaseBundle> query = client.search().forResource(ListResource.class);
        for (Map.Entry<String, String[]> entry : parameterMap.entrySet()) {
            String key = entry.getKey();

            query = query.where(new StringClientParam(key).matches().values(entry.getValue()));
        }
        return query.returnBundle(org.hl7.fhir.r4.model.Bundle.class)
                .execute()
                .getEntry()
                .stream()
                .map(e -> (ListResource) e.getResource())
                .toList();
    }

}
