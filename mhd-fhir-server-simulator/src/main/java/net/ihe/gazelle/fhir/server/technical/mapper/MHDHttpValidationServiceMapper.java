package net.ihe.gazelle.fhir.server.technical.mapper;

import ca.uhn.fhir.rest.api.RestOperationTypeEnum;
import ca.uhn.fhir.rest.api.server.RequestDetails;
import net.ihe.gazelle.evsapi.client.business.ValidationService;
import net.ihe.gazelle.fhir.server.business.ValidatorServiceMapper;
import org.eclipse.microprofile.config.ConfigProvider;

public class MHDHttpValidationServiceMapper implements ValidatorServiceMapper {


    @Override
    public ValidationService getValidationService(RequestDetails requestDetails) {
        String httpValidationName = ConfigProvider.getConfig().getValue("http.validation.name", String.class);
        String httpValidationValidator;
        if (requestDetails.getRestOperationType().equals(RestOperationTypeEnum.TRANSACTION)) {
            httpValidationValidator = ConfigProvider.getConfig().getValue("http.validation.schematron.name-iti-65", String.class);
            return new ValidationService().setValidator(httpValidationValidator).setName(httpValidationName);
        }

        if (requestDetails.getRestOperationType().equals(RestOperationTypeEnum.SEARCH_TYPE) && requestDetails.getResourceName().equals("List")) {
            httpValidationValidator = ConfigProvider.getConfig().getValue("http.validation.schematron.name-iti-66", String.class);
            return new ValidationService().setValidator(httpValidationValidator).setName(httpValidationName);
        }

        if (requestDetails.getRestOperationType().equals(RestOperationTypeEnum.SEARCH_TYPE) && requestDetails.getResourceName().equals("DocumentReference")) {
            httpValidationValidator = ConfigProvider.getConfig().getValue("http.validation.schematron.name-iti-67", String.class);

            return new ValidationService().setValidator(httpValidationValidator).setName(httpValidationName);
        }

        if (requestDetails.getRestOperationType().equals(RestOperationTypeEnum.READ) && requestDetails.getResourceName().equals("DocumentReference")) {
            httpValidationValidator = ConfigProvider.getConfig().getValue("http.validation.schematron.name-iti-68", String.class);

            return new ValidationService().setValidator(httpValidationValidator).setName(httpValidationName);
        }


        return null;


    }
}
