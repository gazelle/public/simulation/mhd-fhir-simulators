package net.ihe.gazelle.fhir.server.technical.mapper;

import ca.uhn.fhir.rest.api.RestOperationTypeEnum;
import ca.uhn.fhir.rest.api.server.RequestDetails;
import net.ihe.gazelle.evsapi.client.business.ValidationService;
import net.ihe.gazelle.fhir.server.business.ValidatorServiceMapper;
import org.eclipse.microprofile.config.ConfigProvider;

public class MHDFhirValidationServiceMapper implements ValidatorServiceMapper {


    @Override
    public ValidationService getValidationService(RequestDetails requestDetails) {
        String fhirValidationName = ConfigProvider.getConfig().getValue("fhir.validation.name", String.class);
        if (requestDetails.getRestOperationType().equals(RestOperationTypeEnum.TRANSACTION)) {
            String fhirValidationValidator = ConfigProvider.getConfig().getValue("fhir.validation.schematron.name-iti-65", String.class);
            return new ValidationService().setValidator(fhirValidationValidator).setName(fhirValidationName);
        } else {

            return null;
        }

    }
}
