package net.ihe.gazelle.fhir.server.technical.ws;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.api.server.RequestDetails;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.gclient.IQuery;
import ca.uhn.fhir.rest.gclient.StringClientParam;
import jakarta.enterprise.context.ApplicationScoped;
import org.eclipse.microprofile.config.ConfigProvider;
import org.hl7.fhir.instance.model.api.IBaseBundle;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.DocumentReference;

import java.util.List;
import java.util.Map;


@ApplicationScoped
public class DocumentReferencesProviderServiceImpl implements DocumentReferencesProviderService {
    private final IGenericClient client;

    public DocumentReferencesProviderServiceImpl() {
        FhirContext ctx = FhirContext.forR4(); // or the appropriate version
        String serverBase = ConfigProvider.getConfig().getValue("nist.fhir.toolkit.endpoint", String.class);
        client = ctx.newRestfulGenericClient(serverBase);
    }

    @Override
    public DocumentReference getDocumentReference(String id) {


        return client.read()
                .resource(DocumentReference.class)
                .withId(id)
                .execute();
    }

    @Override
    public List<DocumentReference> searchDocumentReferences(RequestDetails request) {

        Map<String, String[]> parameterMap = request.getParameters();
        IQuery<IBaseBundle> query = client.search().forResource(DocumentReference.class);
        for (Map.Entry<String, String[]> entry : parameterMap.entrySet()) {
            String key = entry.getKey();
            query = query.where(new StringClientParam(key).matches().values(entry.getValue()));
        }
        return query.returnBundle(org.hl7.fhir.r4.model.Bundle.class)
                .execute()
                .getEntry()
                .stream()
                .map(e -> (DocumentReference) e.getResource())
                .toList();

    }

    @Override
    public Bundle createBundleResource(Bundle bundleDocument) {
        return client.transaction()
                .withBundle(bundleDocument)
                .execute();
    }


}
