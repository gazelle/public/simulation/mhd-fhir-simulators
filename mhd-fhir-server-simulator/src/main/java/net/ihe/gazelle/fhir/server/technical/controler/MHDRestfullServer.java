package net.ihe.gazelle.fhir.server.technical.controler;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.narrative.DefaultThymeleafNarrativeGenerator;
import ca.uhn.fhir.narrative.INarrativeGenerator;
import ca.uhn.fhir.rest.server.IResourceProvider;
import ca.uhn.fhir.rest.server.RestfulServer;
import net.ihe.gazelle.fhir.server.buisness.DocumentReferencesResourceProvider;
import net.ihe.gazelle.fhir.server.buisness.ListResourceProvider;
import net.ihe.gazelle.fhir.server.technical.FhirValidatorInterceptor;
import net.ihe.gazelle.fhir.server.technical.HTTPValidatorInterceptor;
import net.ihe.gazelle.fhir.server.technical.mapper.MHDFhirValidationServiceMapper;
import net.ihe.gazelle.fhir.server.technical.mapper.MHDHttpValidationServiceMapper;
import net.ihe.gazelle.fhir.server.technical.ws.DocumentReferencesProviderServiceImpl;
import net.ihe.gazelle.fhir.server.technical.ws.ListProviderServiceImpl;
import org.eclipse.microprofile.config.ConfigProvider;

import java.util.ArrayList;
import java.util.List;


public class MHDRestfullServer extends RestfulServer {

    private static final long serialVersionUID = 1L;



    public MHDRestfullServer() {
        super(FhirContext.forR4Cached());
    }


    @Override
    public void initialize() {

        List<IResourceProvider> providers = new ArrayList<>();
        providers.add(new DocumentReferencesResourceProvider(new DocumentReferencesProviderServiceImpl()));
        providers.add(new ListResourceProvider(new ListProviderServiceImpl()));
        setResourceProviders(providers);
        INarrativeGenerator narrativeGen = new DefaultThymeleafNarrativeGenerator();
        getFhirContext().setNarrativeGenerator(narrativeGen);

        String evsEndpoint = ConfigProvider.getConfig().getValue("evs.endpoint", String.class);

        registerInterceptor(new HTTPValidatorInterceptor(evsEndpoint, new MHDHttpValidationServiceMapper()));

       registerInterceptor(new FhirValidatorInterceptor(evsEndpoint, new MHDFhirValidationServiceMapper()));


    }

}
