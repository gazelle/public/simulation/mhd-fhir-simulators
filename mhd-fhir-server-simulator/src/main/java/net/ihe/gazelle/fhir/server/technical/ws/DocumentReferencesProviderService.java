package net.ihe.gazelle.fhir.server.technical.ws;


import ca.uhn.fhir.rest.api.server.RequestDetails;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.DocumentReference;

import java.util.List;

public interface DocumentReferencesProviderService {


    DocumentReference getDocumentReference(String id);

    List<DocumentReference> searchDocumentReferences(RequestDetails parameters);

    Bundle createBundleResource(Bundle bundleDocument);
}