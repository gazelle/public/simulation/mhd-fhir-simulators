package net.ihe.gazelle.fhir.server.technical.ws;


import ca.uhn.fhir.rest.api.server.RequestDetails;
import org.hl7.fhir.r4.model.ListResource;

import java.util.List;

public interface ListProviderService {


    List<ListResource> searchListResource(RequestDetails parameters);


}