package net.ihe.gazelle.fhir.server.buisness;


import ca.uhn.fhir.rest.annotation.OptionalParam;
import ca.uhn.fhir.rest.annotation.Search;
import ca.uhn.fhir.rest.api.server.RequestDetails;
import ca.uhn.fhir.rest.param.*;
import ca.uhn.fhir.rest.server.IResourceProvider;
import jakarta.inject.Inject;
import net.ihe.gazelle.fhir.server.technical.ws.ListProviderService;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.DocumentReference;
import org.hl7.fhir.r4.model.ListResource;

import java.util.List;


public class ListResourceProvider implements IResourceProvider {


    protected final ListProviderService listProviderService;


    @Inject
    public ListResourceProvider(ListProviderService listProviderService) {
        this.listProviderService = listProviderService;
    }

    @Override
    public Class<? extends IBaseResource> getResourceType() {
        return ListResource.class;
    }

    @Search()
    public List<ListResource> findListResource(@OptionalParam(name = ListResource.SP_CODE) TokenAndListParam code,
                                               @OptionalParam(name = ListResource.SP_DATE) DateParam date,
                                               @OptionalParam(name = "designationType") TokenAndListParam designationType,
                                               @OptionalParam(name = "identifier") TokenAndListParam identifier,
                                               @OptionalParam(name = DocumentReference.SP_PATIENT) ReferenceAndListParam patient,
                                               @OptionalParam(name = "patient.identifier") TokenAndListParam patientIdentifier,
                                               @OptionalParam(name = "source.given") StringAndListParam sourceGiven,
                                               @OptionalParam(name = "source.family") StringAndListParam sourceFamily,
                                               @OptionalParam(name = "sourceId") TokenAndListParam sourceId,
                                               @OptionalParam(name = ListResource.SP_STATUS) TokenAndListParam status,
                                               RequestDetails requestDetails
    ) {
        return listProviderService.searchListResource(requestDetails);
    }


}
