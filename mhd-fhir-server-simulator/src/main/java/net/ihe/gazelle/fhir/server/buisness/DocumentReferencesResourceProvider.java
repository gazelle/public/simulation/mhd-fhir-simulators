package net.ihe.gazelle.fhir.server.buisness;


import ca.uhn.fhir.rest.annotation.*;
import ca.uhn.fhir.rest.api.server.RequestDetails;
import ca.uhn.fhir.rest.param.DateParam;
import ca.uhn.fhir.rest.param.ReferenceAndListParam;
import ca.uhn.fhir.rest.param.StringAndListParam;
import ca.uhn.fhir.rest.param.TokenAndListParam;
import ca.uhn.fhir.rest.server.IResourceProvider;
import jakarta.inject.Inject;
import net.ihe.gazelle.fhir.server.technical.ws.DocumentReferencesProviderService;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.instance.model.api.IIdType;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.DocumentReference;

import java.util.List;

public class DocumentReferencesResourceProvider implements IResourceProvider {


    protected final DocumentReferencesProviderService documentReferencesProviderService;


    @Inject
    public DocumentReferencesResourceProvider(DocumentReferencesProviderService patientResourceProviderService) {
        this.documentReferencesProviderService = patientResourceProviderService;
    }

    @Override
    public Class<? extends IBaseResource> getResourceType() {
        return DocumentReference.class;

    }

    @Read
    public DocumentReference readDocumentReference(@IdParam IIdType theId) {
        return documentReferencesProviderService.getDocumentReference(theId.getValue());
    }

    @Search()
    public List<DocumentReference> findDocumentReference(
            @OptionalParam(name = "author.given") StringAndListParam authorGiven,
            @OptionalParam(name = "author.family") StringAndListParam authorFamily,
            @OptionalParam(name = DocumentReference.SP_CATEGORY) TokenAndListParam category,
            @OptionalParam(name = "creation") DateParam creation,
            @OptionalParam(name = DocumentReference.SP_DATE) DateParam date,
            @OptionalParam(name = DocumentReference.SP_EVENT) TokenAndListParam event,
            @OptionalParam(name = DocumentReference.SP_FACILITY) TokenAndListParam facilty,
            @OptionalParam(name = DocumentReference.SP_FORMAT) TokenAndListParam format,
            @OptionalParam(name = DocumentReference.SP_IDENTIFIER) TokenAndListParam identifier,
            @OptionalParam(name = DocumentReference.SP_PATIENT) ReferenceAndListParam patient,
            @OptionalParam(name = "patient.identifier") TokenAndListParam patientIdentifier,
            @OptionalParam(name = DocumentReference.SP_PERIOD) DateParam period,
            @OptionalParam(name = DocumentReference.SP_RELATED) ReferenceAndListParam related,
            @OptionalParam(name = DocumentReference.SP_SECURITY_LABEL) TokenAndListParam securityLabel,
            @OptionalParam(name = DocumentReference.SP_SETTING) TokenAndListParam setting,
            @OptionalParam(name = DocumentReference.SP_STATUS) TokenAndListParam status,
            @OptionalParam(name = DocumentReference.SP_TYPE) TokenAndListParam type,
            RequestDetails requestDetails) {

        return documentReferencesProviderService.searchDocumentReferences(requestDetails);
    }

    @Transaction()
    public Bundle createDocumentReference(@TransactionParam Bundle bundle) {
        return documentReferencesProviderService.createBundleResource(bundle);
    }


}
