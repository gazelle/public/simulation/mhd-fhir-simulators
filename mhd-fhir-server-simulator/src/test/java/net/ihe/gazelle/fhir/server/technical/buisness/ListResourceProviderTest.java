package net.ihe.gazelle.fhir.server.technical.buisness;


import ca.uhn.fhir.rest.api.server.RequestDetails;
import net.ihe.gazelle.fhir.server.buisness.ListResourceProvider;
import net.ihe.gazelle.fhir.server.technical.ws.ListProviderService;
import org.hl7.fhir.r4.model.ListResource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class ListResourceProviderTest {
    private ListResourceProvider provider;
    private ListProviderService service;
    private RequestDetails requestDetails;

    @BeforeEach
    public void setup() {
        service = mock(ListProviderService.class);
        provider = new ListResourceProvider(service);
        requestDetails = mock(RequestDetails.class);
    }

    @Test
    public void testFindListResource() {
        ListResource expectedListResource = new ListResource();
        when(service.searchListResource(requestDetails)).thenReturn(Collections.singletonList(expectedListResource));

        List<ListResource> listResources = provider.findListResource(null, null, null, null, null, null, null, null, null, null, requestDetails);

        verify(service, times(1)).searchListResource(requestDetails);
        assertEquals(Collections.singletonList(expectedListResource), listResources);
    }

    // Add similar test methods for other methods in the ListResourceProvider class
}