package net.ihe.gazelle.fhir.server.technical.mapper;

import ca.uhn.fhir.rest.api.RestOperationTypeEnum;
import ca.uhn.fhir.rest.api.server.RequestDetails;
import net.ihe.gazelle.evsapi.client.business.ValidationService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

public class MHDHttpValidationServiceMapperTest {
    private MHDHttpValidationServiceMapper mapper;
    private RequestDetails requestDetails;

    @BeforeEach
    public void setup() {
        mapper = new MHDHttpValidationServiceMapper();
        requestDetails = Mockito.mock(RequestDetails.class);
    }

    @Test
    public void testGetValidationServiceForTransaction() {
        when(requestDetails.getRestOperationType()).thenReturn(RestOperationTypeEnum.TRANSACTION);

        ValidationService validationService = mapper.getValidationService(requestDetails);

        assertEquals("CH_ITI-65-ProvideDocumentBundle_POST_MHD_Request", validationService.getValidator());
        assertEquals("HTTP Message Validator", validationService.getName());
    }




    @Test
    public void testGetValidationServiceForSearchList() {
        when(requestDetails.getRestOperationType()).thenReturn(RestOperationTypeEnum.SEARCH_TYPE);
        when(requestDetails.getResourceName()).thenReturn("List");

        ValidationService validationService = mapper.getValidationService(requestDetails);

        assertEquals("CH_ITI-66-FindDocumentLists-GET_MHD_Request", validationService.getValidator());
        assertEquals("HTTP Message Validator", validationService.getName());
    }

    @Test
    public void testGetValidationServiceForSearchDocumentReference() {
        when(requestDetails.getRestOperationType()).thenReturn(RestOperationTypeEnum.SEARCH_TYPE);
        when(requestDetails.getResourceName()).thenReturn("DocumentReference");

        ValidationService validationService = mapper.getValidationService(requestDetails);

        assertEquals("CH_ITI-67-FindDocumentReferences-GET_MHD_Request", validationService.getValidator());
        assertEquals("HTTP Message Validator", validationService.getName());
    }

    @Test
    public void testGetValidationServiceForReadDocumentReference() {
        when(requestDetails.getRestOperationType()).thenReturn(RestOperationTypeEnum.READ);
        when(requestDetails.getResourceName()).thenReturn("DocumentReference");

        ValidationService validationService = mapper.getValidationService(requestDetails);

        assertEquals("CH_ITI-68-RetrieveDocument-GET_MHD_Request", validationService.getValidator());
        assertEquals("HTTP Message Validator", validationService.getName());
    }


}