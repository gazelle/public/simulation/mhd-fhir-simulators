package net.ihe.gazelle.fhir.server.technical.buisness;


import ca.uhn.fhir.rest.api.server.RequestDetails;
import net.ihe.gazelle.fhir.server.buisness.DocumentReferencesResourceProvider;
import net.ihe.gazelle.fhir.server.technical.ws.DocumentReferencesProviderService;
import org.hl7.fhir.r4.model.DocumentReference;
import org.hl7.fhir.r4.model.IdType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class DocumentReferencesResourceProviderTest {
    private DocumentReferencesResourceProvider provider;
    private DocumentReferencesProviderService service;
    private RequestDetails requestDetails;

    @BeforeEach
    public void setup() {
        service = mock(DocumentReferencesProviderService.class);
        provider = new DocumentReferencesResourceProvider(service);
        requestDetails = mock(RequestDetails.class);
    }

    @Test
    public void testFindDocumentReference() {
        DocumentReference expectedDocumentReference = new DocumentReference();
        when(service.searchDocumentReferences(requestDetails)).thenReturn(Collections.singletonList(expectedDocumentReference));

        List<DocumentReference> documentReferences = provider.findDocumentReference(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, requestDetails);

        verify(service, times(1)).searchDocumentReferences(requestDetails);
        assertEquals(Collections.singletonList(expectedDocumentReference), documentReferences);
    }

    @Test
    public void testReadDocumentReference() {
        DocumentReference expectedDocumentReference = new DocumentReference();
        when(service.getDocumentReference("1")).thenReturn(expectedDocumentReference);

        DocumentReference documentReference = provider.readDocumentReference(new IdType("1"));

        verify(service, times(1)).getDocumentReference("1");
        assertEquals(expectedDocumentReference, documentReference);
    }

    // Add similar test methods for other methods in the DocumentReferencesResourceProvider class
}