package net.ihe.gazelle.fhir.server.technical.mapper;

import ca.uhn.fhir.rest.api.RestOperationTypeEnum;
import ca.uhn.fhir.rest.api.server.RequestDetails;
import net.ihe.gazelle.evsapi.client.business.ValidationService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

public class MHDFhirValidationServiceMapperTest {
    private MHDFhirValidationServiceMapper mapper;
    private RequestDetails requestDetails;

    @BeforeEach
    public void setup() {
        mapper = new MHDFhirValidationServiceMapper();
        requestDetails = Mockito.mock(RequestDetails.class);
    }

    @Test
    public void testGetValidationServiceForTransaction() {
        when(requestDetails.getRestOperationType()).thenReturn(RestOperationTypeEnum.TRANSACTION);

        ValidationService validationService = mapper.getValidationService(requestDetails);

        assertEquals("http://fhir.ch/ig/ch-epr-fhir/StructureDefinition/ch-mhd-providedocumentbundle-comprehensive|4.0.1-ballot", validationService.getValidator());
        assertEquals("Matchbox", validationService.getName());
    }
}
