# mhd-fhir-simulator
This project is composed of two subprojects:
- mhd-fhir-server-simulator
- mhd-fhir-client-simulator

## mhd-fhir-server-simulator
This project uses Quarkus, the Supersonic Subatomic Java Framework.

MHD manage two kinds of resources : `Document` and `DocumentReference.
This project handles two kinds of requests:

- `GET /Document/{id}` : Get a document by id.
- `GET/POST /Document?` : Search for patients.

THe app has one servlet:
- `CH` : The CH servlet is used to simulate the CH FHIR server.


## mhd-fhir-client-simulator
To be defined.